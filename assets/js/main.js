"Use Strict";

$(document).ready(function(){

  if(document.documentElement.scrollTop > 3){
    $("#logo").css({
      "margin-top": ".8em",
      "margin-bottom": "-2.5em"
    });
  }

  setHomeMargin();

  setActiveState();
  
  $(window).scroll(function(){
    setActiveState();
  });
});


function setHomeMargin(){
  let header = document.querySelector("header")
  let margin = header.clientHeight + Number(header.style.paddingBottom) + 15;
  document.getElementById("home").style.marginTop = margin + "px";
}


function setActiveState(){

  let $sections = $('.card');
  let currentScroll = $(this).scrollTop();
  let $currentSection;
  
  $sections.each(function(){
  
    let divPosition = $(this).offset().top;
    
    if( (divPosition - (document.documentElement.clientHeight / 2)) < currentScroll ){
      
      $currentSection = $(this);
      
    }
    
    let id = $currentSection.attr("id");
    $(".card").removeClass("active");
    $("#" + id).addClass("active");

    let previous = document.documentElement.getElementsByClassName("nav-current");
    for(let i = 0; i < previous.length; i++){
      previous[i].classList.remove("nav-current");
    }
  
    let navBtns = document.documentElement.getElementsByClassName("nav-btn");

    switch (id){
      case "home":
        navBtns[0].classList.add("nav-current");
        break;
      case "about":
        navBtns[1].classList.add("nav-current");
        break;
      case "gallery":
        navBtns[2].classList.add("nav-current");
        break;
      case "contact":
        navBtns[3].classList.add("nav-current");
    }  
  });
}


function gotoHome() {

  if(document.getElementById("nav-btns").style.display == "flex"){
    if(checkMobile()){
      toggleMenu();
    }
  }

  $("html, body").animate({
    scrollTop: 0
  }, 500);

}


function gotoAbout() {

  if(checkMobile()){
    toggleMenu();
  }

  setTimeout(function(){
    if(document.documentElement.scrollTop < 5){
      $("html, body").scrollTop(3);
    }
  
    setTimeout(function(){
      let headerHeight = document.getElementsByTagName("header")[0].offsetHeight;
      //let position = $("#about").offset().top - ((document.documentElement.clientHeight - $("#about").height()) / 2);
      let position = $("#about").offset().top - headerHeight;
      //$(window).scrollTop(position);
      $("html, body").animate({
        scrollTop: position
      }, 500);
    }, 110);
  }, 300);

}


function gotoGallery() {

  if(checkMobile()){
    toggleMenu();
  }

  setTimeout(function(){
    if(document.documentElement.scrollTop < 5){
      $("html, body").scrollTop(3);
    }
  
    setTimeout(function(){
      let headerHeight = document.getElementsByTagName("header")[0].offsetHeight;
      //let position = $("#about").offset().top - ((document.documentElement.clientHeight - $("#about").height()) / 2);
      let position = $("#gallery").offset().top - headerHeight;
      //$(window).scrollTop(position);
      $("html, body").animate({
        scrollTop: position
      }, 500);
    }, 100);  
  }, 300);

}


function gotoContact() {

  if(checkMobile()){
    toggleMenu();
  }

  setTimeout(function(){
    if(document.documentElement.scrollTop < 5){
      $("html, body").scrollTop(3);
    }
  
    setTimeout(function(){
      let headerHeight = document.getElementsByTagName("header")[0].offsetHeight;
      //let position = $("#about").offset().top - ((document.documentElement.clientHeight - $("#about").height()) / 2);
      let position = $("#contact").offset().top - headerHeight;
      //$(window).scrollTop(position);
      $("html, body").animate({
        scrollTop: position
      }, 500);
    }, 100);
  }, 300);

}


function toggleMenu(){
  hide(document.getElementById("nav-btns"));
  toggleHamburgerIcon(document.getElementById("hamburger-menu"));
}


function checkMobile(){
  if(document.documentElement.clientWidth <= 600){
    return true;
  } else {
    return false;
  }
}


document.addEventListener("scroll", function(){
  if (document.documentElement.clientWidth > 600){
    if(document.documentElement.scrollTop > 2){
      $("#logo").stop(true).animate({
        //"width": "5%",
        //"margin-left": "46.5%",
        "margin-top": ".8em",
        "margin-bottom": "-2.5em"
      }, 100);
      //document.getElementById("header").style.boxShadow = "0 5px 15px #aaaaaa";
    }else if(document.documentElement.scrollTop <= 2){
      $("#logo").stop(true).animate({
        //"width": "8%",
        //"margin-left": "45%",
        "margin-top": "2em",
        "margin-bottom": ".5em"
      }, 100);
      //document.getElementById("header").style.boxShadow = "none";
    }
  }
});


function toggleHamburgerIcon(icon){
  switch (icon.classList[0]){
    case "play":
      icon.classList.toggle('reverse');
      icon.classList.toggle('play');
      break;
    case "reverse":
      icon.classList.toggle('play');
      icon.classList.toggle('reverse');
      break;
    default:
      icon.classList.toggle('play');
  }
}


document.getElementById("hamburger-menu").addEventListener("click", function(){

  toggleHamburgerIcon(this);

  let navBtns = document.getElementById("nav-btns");
  toggle(navBtns);

  /* if(navBtns.style.display != "block") {
      navBtns.style.display = "block";
  } else {
      navBtns.style.display = "none";
  } */

});


function toggle(navBtns) {
  if (navBtns.classList.contains("visible")){
    hide(navBtns);
  } else {
    show(navBtns);
  }
}


function show (element) {

	let getHeight = function () {
		element.style.display = 'block'; // Make it visible
		var height = element.scrollHeight + 'px'; // Get it's height
		element.style.display = ''; //  Hide it again
		return height;
	};

	let height = getHeight(); // Get the natural height
	element.classList.add("visible"); // Make the element visible
  element.style.height = height; // Update the max-height
  element.style.overflow = "initial";

	// Once the transition is complete, remove the inline max-height so the content can scale responsively
	window.setTimeout(function () {
    element.style.height = '';
    
	}, 300);

};


function hide(element) {

  element.style.overflow = "hidden";
	// Give the element a height to change from
	element.style.height = element.scrollHeight + 'px';

	// Set the height back to 0
	window.setTimeout(function () {
		element.style.height = '0';
	}, 1);

	// When the transition is complete, hide it
	window.setTimeout(function () {
    element.classList.remove('visible');
  }, 600);

};


function openModal(img){
  document.getElementById("modal").style.display = "block";
  document.getElementById("modal-img").src = img.src;
  document.getElementById("caption").innerHTML = img.alt;
}


function closeModal(){
  document.getElementById("modal").style.display = "none";
}